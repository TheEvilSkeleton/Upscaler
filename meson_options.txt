option (
    'profile',
    type: 'combo',
    choices: [
        'default',
        'development'
    ],
    value: 'default'
)

option (
    'network_tests',
    type: 'boolean',
    value: true,
    description: 'Allow networking in unit-tests'
)
